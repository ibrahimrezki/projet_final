resource "azurerm_resource_group" "resourcegroup" {
  name     = "${var.name1["0"]}"
  location = "${var.location1["0"]}"
}
resource "azurerm_virtual_network" "virtualnetwork" {
  name                = "${var.name2["0"]}"
  resource_group_name = "${azurerm_resource_group.resourcegroup.name}"
  address_space       = "${var.address_space1}"
  location            = "${var.location1["0"]}"
}
resource "azurerm_subnet" "subnet" {
  name                 = "${var.name3["0"]}"
  resource_group_name  = "${azurerm_resource_group.resourcegroup.name}"
  virtual_network_name = "${azurerm_virtual_network.virtualnetwork.name}"
  address_prefix       = "${var.address_space2}"
}
resource "azurerm_public_ip" "publicip" {
  name                         = "${var.name11["0"]}"
  location                     = "${var.location1["0"]}"
  resource_group_name          = "${azurerm_resource_group.resourcegroup.name}"
  public_ip_address_allocation = "${var.address_type1["1"]}"
  domain_name_label            = "cloudvm10"
}
resource "azurerm_network_interface" "networkinterface" {
  count               = "3"
  name                = "${element(var.name4, count.index + 1)}"
  location            = "${azurerm_resource_group.resourcegroup.location}"
  resource_group_name = "${azurerm_resource_group.resourcegroup.name}"
  ip_configuration {
    name                          = "${element(var.name5, count.index + 1)}"
    subnet_id                     = "${azurerm_subnet.subnet.id}"
    private_ip_address_allocation = "${var.address_type1["0"]}"
    private_ip_address            = "${element(var.address_space3, count.index + 1)}"
  }
}
resource "azurerm_network_interface" "networkinterface3" {
  name                = "${var.name4["0"]}"
  location            = "${azurerm_resource_group.resourcegroup.location}"
  resource_group_name = "${azurerm_resource_group.resourcegroup.name}"
  ip_configuration {
    name                          = "${var.name5["0"]}"
    subnet_id                     = "${azurerm_subnet.subnet.id}"
    private_ip_address_allocation = "${var.address_type1["1"]}"
    public_ip_address_id          = "${azurerm_public_ip.publicip.id}"
  }
}
resource "azurerm_network_security_group" "securitygroup" {
  name                = "${var.name6["0"]}"
  location            = "${azurerm_resource_group.resourcegroup.location}"
  resource_group_name = "${azurerm_resource_group.resourcegroup.name}"
  security_rule {
    name                       = "${var.name7["0"]}"
    priority                   = "${var.priority1["0"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["0"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["1"]}"
    priority                   = "${var.priority1["1"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["1"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["2"]}"
    priority                   = "${var.priority1["2"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["2"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["3"]}"
    priority                   = "${var.priority1["3"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["3"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["4"]}"
    priority                   = "${var.priority1["4"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["4"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["5"]}"
    priority                   = "${var.priority1["5"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["5"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["6"]}"
    priority                   = "${var.priority1["6"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["6"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["7"]}"
    priority                   = "${var.priority1["7"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["7"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["8"]}"
    priority                   = "${var.priority1["8"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["8"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = "${azurerm_resource_group.resourcegroup.name}"
    }
    byte_length = 8
}
resource "azurerm_storage_account" "storageaccount" {
  name                     = "${var.name8["0"]}"
  resource_group_name      = "${azurerm_resource_group.resourcegroup.name}"
  location                 = "${var.location1["0"]}"
  account_replication_type = "${var.account1}"
  account_tier             = "${var.account2}"
}
resource "azurerm_virtual_machine" "virtual_machine" {
  count                 = "3"
  name                  = "${element(var.name9, count.index + 1)}"
  location              = "${var.location1["0"]}"
  resource_group_name   = "${azurerm_resource_group.resourcegroup.name}"
  network_interface_ids = ["${element(azurerm_network_interface.networkinterface.*.id, count.index)}"]
  vm_size               = "${element(var.size, count.index + 1)}"
  storage_os_disk {
    name              = "${element(var.name10, count.index + 1)}"
    caching           = "${var.caching1}"
    create_option     = "${var.creat1}"
    managed_disk_type = "${var.managed1}"
  }
  storage_image_reference {
    publisher = "${var.publi1}"
    offer     = "${var.offer1}"
    sku       = "${var.sku1}"
    version   = "${var.version}"
  }
  os_profile {
    computer_name  = "${element(var.computer_name1, count.index + 1)}"
    admin_username = "${var.admin_username1}"
    admin_password = "${var.admin_password1}"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  boot_diagnostics {
    enabled     = "${var.enabled1}"
    storage_uri = "${azurerm_storage_account.storageaccount.primary_blob_endpoint}"
    }
}
resource "azurerm_virtual_machine" "virtual_machine3" {
  name                  = "${var.name9["0"]}"
  location              = "${var.location1["0"]}"
  resource_group_name   = "${azurerm_resource_group.resourcegroup.name}"
  network_interface_ids = ["${azurerm_network_interface.networkinterface3.id}"]
  vm_size               = "${var.size["0"]}"
  storage_os_disk {
    name              = "${var.name10["0"]}"
    caching           = "${var.caching1}"
    create_option     = "${var.creat1}"
    managed_disk_type = "${var.managed1}"
  }
  storage_image_reference {
    publisher = "${var.publi1}"
    offer     = "${var.offer1}"
    sku       = "${var.sku1}"
    version   = "${var.version}"
  }
  os_profile {
    computer_name  = "${var.computer_name1["0"]}"
    admin_username = "${var.admin_username1}"
    admin_password = "${var.admin_password1}"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  boot_diagnostics {
    enabled     = "${var.enabled1}"
    storage_uri = "${azurerm_storage_account.storageaccount.primary_blob_endpoint}"
  }
  provisioner "file" {
    source = "./startVM1.sh"
    destination = "/home/stage/startVM1.sh"

    connection {
      type = "ssh"
      user = "${var.admin_username1}"
      password = "${var.admin_password1}"
    }
  }
  provisioner "file" {
    source = "./Readme.txt"
    destination = "/home/stage/Readme.txt"

    connection {
      type = "ssh"
      user = "${var.admin_username1}"
      password = "${var.admin_password1}"
    }
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/stage/startVM1.sh",
      "cd /home/stage",
      "sudo /home/stage/startVM1.sh"
          ]
    connection {
      type     = "ssh"
      user     = "${var.admin_username1}"
      password = "${var.admin_password1}"
    }
  }
}
resource "azurerm_resource_group" "resourcegroup2" {
  name     = "${var.name1["1"]}"
  location = "${var.location1["1"]}"
}
resource "azurerm_virtual_network" "virtualnetwork2" {
  name                = "${var.name2["1"]}"
  resource_group_name = "${azurerm_resource_group.resourcegroup2.name}"
  address_space       = "${var.address_space1}"
  location            = "${var.location1["1"]}"
}
resource "azurerm_subnet" "subnet2" {
  name                 = "${var.name3["1"]}"
  resource_group_name  = "${azurerm_resource_group.resourcegroup2.name}"
  virtual_network_name = "${azurerm_virtual_network.virtualnetwork2.name}"
  address_prefix       = "${var.address_space2}"
}
resource "azurerm_public_ip" "publicip2" {
  name                         = "${var.name11["1"]}"
  location                     = "${var.location1["1"]}"
  resource_group_name          = "${azurerm_resource_group.resourcegroup2.name}"
  public_ip_address_allocation = "${var.address_type1["1"]}"
  domain_name_label            = "cloudvm50"
}
resource "azurerm_network_interface" "networkinterface2" {
  name                = "${var.name4["4"]}"
  location            = "${azurerm_resource_group.resourcegroup2.location}"
  resource_group_name = "${azurerm_resource_group.resourcegroup2.name}"
  ip_configuration {
    name                          = "${var.name5["4"]}"
    subnet_id                     = "${azurerm_subnet.subnet2.id}"
    private_ip_address_allocation = "${var.address_type1["1"]}"
    public_ip_address_id          = "${azurerm_public_ip.publicip2.id}"
  }
}
resource "azurerm_network_security_group" "securitygroup2" {
  name                = "${var.name6["1"]}"
  location            = "${azurerm_resource_group.resourcegroup2.location}"
  resource_group_name = "${azurerm_resource_group.resourcegroup2.name}"
  security_rule {
    name                       = "${var.name7["0"]}"
    priority                   = "${var.priority1["0"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["0"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["1"]}"
    priority                   = "${var.priority1["1"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["1"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["2"]}"
    priority                   = "${var.priority1["2"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["2"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["3"]}"
    priority                   = "${var.priority1["3"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["3"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["4"]}"
    priority                   = "${var.priority1["4"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["4"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["7"]}"
    priority                   = "${var.priority1["7"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["7"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "${var.name7["8"]}"
    priority                   = "${var.priority1["8"]}"
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "${var.destination_port1["8"]}"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}
resource "random_id" "randomId2" {
  keepers = {
    # Generate a new ID only when a new resource group is defined
    resource_group = "${azurerm_resource_group.resourcegroup2.name}"
  }
  byte_length = 8
}
resource "azurerm_storage_account" "storageaccount2" {
  name                     = "${var.name8["1"]}"
  resource_group_name      = "${azurerm_resource_group.resourcegroup2.name}"
  location                 = "${var.location1["1"]}"
  account_replication_type = "${var.account1}"
  account_tier             = "${var.account2}"
}
resource "azurerm_virtual_machine" "virtual_machine2" {
  name                   = "${var.name9["4"]}"
  location               = "${var.location1["1"]}"
  resource_group_name    = "${azurerm_resource_group.resourcegroup2.name}"
  network_interface_ids  = ["${azurerm_network_interface.networkinterface2.id}"]
  vm_size                = "${var.size["4"]}"
  storage_os_disk {
    name              = "${var.name10["4"]}"
    caching           = "${var.caching1}"
    create_option     = "${var.creat1}"
    managed_disk_type = "${var.managed1}"
  }
  storage_image_reference {
    publisher = "${var.publi1}"
    offer     = "${var.offer1}"
    sku       = "${var.sku1}"
    version   = "${var.version}"
  }
  os_profile {
    computer_name  = "${var.computer_name1["4"]}"
    admin_username = "${var.admin_username1}"
    admin_password = "${var.admin_password1}"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  boot_diagnostics {
    enabled     = "${var.enabled1}"
    storage_uri = "${azurerm_storage_account.storageaccount2.primary_blob_endpoint}"
  }
  provisioner "remote-exec" {
    inline = [
      "cd /home/stage",
          ]
    connection {
      type     = "ssh"
      user     = "${var.admin_username1}"
      password = "${var.admin_password1}"
    }
  }
}
