output "devops_app_01_DNS" {
  value = "${azurerm_public_ip.publicip.fqdn}"
}
output "devops_app_01_IP" {
  value = "${azurerm_public_ip.publicip.ip_address}"
}
output "devops_app_05_DNS" {
  value = "${azurerm_public_ip.publicip2.fqdn}"
}
output "devops_app_05_IP" {
  value = "${azurerm_public_ip.publicip2.ip_address}"
}
