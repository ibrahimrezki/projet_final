#!/bin/bash
sudo apt-get update
echo y | sudo apt-get upgrade

# Installation of Jenkins
echo y | sudo apt-get install sudo unzip vim tar openssh-server
sudo wget -P /opt http://mirrors.jenkins.io/war-stable/latest/jenkins.war
chown -R stage:stage /opt/jenkins.war

# Create Jenkins Daemon
touch jenkins.service
echo "[Unit]" >>jenkins.service
echo "Description=Jenkins Daemon" >>jenkins.service
echo >>jenkins.service
echo "[Service]" >>jenkins.service
echo "ExecStart=/home/stage/jdk1.8.0_181/bin/java -jar /opt/jenkins.war --httpPort=8085" >>jenkins.service
echo "User=stage" >>jenkins.service
echo >>jenkins.service
echo "[Install]" >>jenkins.service
echo "WantedBy=multi-user.target" >>jenkins.service

# Create Artifactory Daemon
touch artifactory.service
echo "[Unit]" >>artifactory.service
echo "Description=Artifactory Daemon" >>artifactory.service
echo >>artifactory.service
echo "[Service]" >>artifactory.service
echo "ExecStart=/home/stage/artifactory-oss-6.4.0/bin/artifactory.sh" >>artifactory.service
echo "User=stage" >>artifactory.service
echo >>artifactory.service
echo "[Install]" >>artifactory.service
echo "WantedBy=multi-user.target" >>artifactory.service

# Create Tomcat Daemon
touch tomcat.service
echo "[Unit]" >>tomcat.service
echo "Description=Tomcat Daemon" >>tomcat.service
echo >>tomcat.service
echo "[Service]" >>tomcat.service
echo "ExecStart=/home/stage/apache-tomcat-9.0.12/bin/startup.sh" >>tomcat.service
echo "ExecStop=/home/stage/apache-tomcat-9.0.12/bin/shutdown.sh" >>tomcat.service
echo "User=stage" >>tomcat.service
echo >>tomcat.service
echo "[Install]" >>tomcat.service
echo "WantedBy=multi-user.target" >>tomcat.service

# Create Gitlab Daemon
touch gitlab.service
echo "[Unit]" >>gitlab.service
echo "Description=Gitlab Daemon" >>gitlab.service
echo >>gitlab.service
echo "[Service]" >>gitlab.service
echo "ExecStart=/home/stage/gitlabstart.sh" >>gitlab.service
echo "ExecStop=/home/stage/gitlabstop.sh" >>gitlab.service
echo "User=stage" >>gitlab.service
echo >>gitlab.service
echo "[Install]" >>gitlab.service
echo "WantedBy=multi-user.target" >>gitlab.service

# Create startup script for docker daemon
echo "#!/bin/bash" >>gitlabstart.sh
echo "sudo gitlab-ctl start" >>gitlabstart.sh

# Create shutdown script for docker daemon
echo "#!/bin/bash" >>gitlabstop.sh
echo "sudo gitlab-ctl stop" >>gitlabstop.sh

# Create Docker Daemon
touch container.service
echo "[Unit]" >>container.service
echo "Description=Docker Daemon" >>container.service
echo >>container.service
echo "[Service]" >>container.service
echo "ExecStart=/home/stage/dockerstart.sh" >>container.service
echo "ExecStop=/home/stage/dockerstop.sh" >>container.service
echo "User=stage" >>container.service
echo >>container.service
echo "[Install]" >>container.service
echo "WantedBy=multi-user.target" >>container.service

# Create startup script for docker daemon
echo "#!/bin/bash" >>dockerstart.sh
echo "sudo docker run -dit --hostname VM_5_CI --name docker-ci -p 8090:22 -p 8091:8080 maven /bin/bash" >>dockerstart.sh
echo "sudo docker run -dit --hostname VM_5_CD --name docker-cd -p 8092:22 -p 8093:8080 centos /bin/bash" >>dockerstart.sh

# Create shutdown script for docker daemon
echo "#!/bin/bash" >>dockerstop.sh
echo "sudo docker stop docker-ci" >>dockerstop.sh
echo "sudo docker stop docker-cd" >>dockerstop.sh

# Changing ownership and properties of services
sudo chmod 0775 jenkins.service
sudo chmod 0775 artifactory.service
sudo chmod 0775 container.service
sudo chmod 0775 dockerstart.sh
sudo chmod 0775 dockerstop.sh

# Generation of public ssh-key for user "stage"
sudo ssh-keygen -N "" -f /root/.ssh/id_rsa
sudo cp /root/.ssh/id_rsa.pub /root/.ssh/authorized_keys

# Download Git respository
git clone https://gitlab.com/jcc1980/gtm.git

# Installation of JDK 8
cd /home/stage
sudo wget -P /home/stage --no-cookies --no-check-certificate -c --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u181-b13/96a7b8442fe848ef90c96a2fad6ed6d1/jdk-8u181-linux-x64.tar.gz
tar -xzf jdk-8u181-linux-x64.tar.gz

################ Do the following commands ########################
# sudo passwd root
# su
# update-alternatives --install /usr/bin/java java /home/stage/jdk1.8.0_181/bin/java 2000
# update-alternatives --install /usr/bin/javac javac /home/stage/jdk1.8.0_181/bin/javac 2000
# update-alternatives --install /usr/bin/jar jar /home/stage/jdk1.8.0_181/bin/jar 2000
# echo JRE_HOME="/home/stage/jdk1.8.0_181" >>/etc/environment
# echo JAR_HOME="/home/stage/jdk1.8.0_181" >>/etc/environment
# echo JAVA_HOME="/home/stage/jdk1.8.0_181" >>/etc/environment
# source /etc/environment 
# PATH=/home/stage/jdk1.8.0_181/bin:$PATH; export PATH
# echo $JRE_HOME $JAR_HOME $JAVA_HOME
# rm /home/stage/jdk-8u181-linux-x64.tar.gz

# Installation of Ansible
# apt-get update
# apt-get install software-properties-common
# apt-add-repository ppa:ansible/ansible
# apt-get update
# echo y | apt-get install ansible
# echo "host_key_checking = False" >>/etc/ansible/ansible.cfg

# Adjust several ownerships
# chown root:root jenkins.service artifactory.service container.service tomcat.service gitlab.service gitlabstart.sh gitlabstop.sh dockerstart.sh dockerstop.sh
# chown -R stage:stage jdk1.8.0_181/

# Start Jenkins
# mv jenkins.service /etc/systemd/system/
# systemctl enable jenkins.service
# systemctl daemon-reload
# systemctl start jenkins.service

# Reminder: Provide public key to all machines with "ssh-copy-id", log in to the virtual machine and copy public key to root/.ssh directory. Make an apt-get update and apt-get upgrade for all machines.
